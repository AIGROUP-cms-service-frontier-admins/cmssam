%define site org.cms
%define dir %{_libexecdir}/grid-monitoring/probes
%define ncgx /usr/lib/ncgx/x_plugins

Summary: WLCG Compliant Probes from %{site}
Name: nagios-plugins-wlcg-org.cms
Version: 1.1.58
Release: 1%{?dist}

License: GPL
Group: Network/Monitoring
Source0: %{name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: gfal2-all
Requires: gfal2-python
Requires: python >= 2.4
Requires: python-GridMon >= 1.1.3
Requires: python-ldap
Requires: python-suds >= 0.3.5
Requires: python-nap
Requires: xrootd-client
Requires: xrootd-python
AutoReqProv: no
BuildArch: noarch

%description
Contains the CMS-specific probes and metrics.

%prep
%setup -q

%build

%install
%{__rm} -rf %{buildroot}
install --directory %{buildroot}/%{dir}
install --directory %{buildroot}/%{ncgx}
install --directory %{buildroot}/etc/cron.d
%{__cp} -rpf %{site} %{buildroot}%{dir}
%{__cp} -rpf config/etf_plugin_cms.py %{buildroot}%{ncgx}
%{__cp} -rpf config/cms_glexec-etf %{buildroot}/etc/cron.d/cms_glexec

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{dir}/%{site}
%{ncgx}/etf_plugin_cms.py
%{ncgx}/etf_plugin_cms.pyc
%{ncgx}/etf_plugin_cms.pyo
/etc/cron.d/cms_glexec

%changelog
* Thu Jan 9 2020 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.59-1.
- Removed the remotestageout test
* Wed Apr 17 2019 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.58-1.
- Fixed environment problems in remotestageout and mc test
- Fixed bug in squid test
* Fri Apr 5 2019 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.57-1.
- Fixed SCRAM_ARCH conflict with Singularity; new squid test
* Fri Feb 22 2019 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.56-1.
- Improved timeout logic to xrootd-fallback
* Fri Feb 1 2019 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.55-1.
- Made xrootd fallback test critical
* Thu Dec 6 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.54-1.
- fixes in xrootd-fallback and moved CMSSW to CMSSW_9_2_6 in tests
* Tue Nov 6 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.53-1.
- fixes in xrootd-fallback, remotestageout, mc and SRM tests
* Fri Jul 20 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.52-1.
- moved isolation test to lcgadmin
- disabled pilot role submission
- shorter summary for xrootd test
- removed surl printout in summary of SE tests
- added /storage to the paths to mount in Singularity
- copy proxy inside Singularity
- tweak to library search in xrootd tests
- moved several tests inside Singularity
* Fri May 4 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.51-1.
- added workaround in etf plugin for xrootd tag
* Thu May 3 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.50-1.
- added xrootd probe
- new etf plugin to remove usage of BDII
- made test file name for mc test more random
- removed gfal-copy check from CE-cms-env
* Wed Apr 18 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.49-1.
- glexec test eliminated from isolation test
- removed double binds in singularity
* Mon Mar 5 2018 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.48-1.
- Better error reporting in mc test
- mc test runs in Singularity if available
- xrootd storage tests supported
* Wed Nov 1 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.47-1.
- Fixed a bug that prevented the renewal of the production proxy
* Mon Oct 30 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.46-1.
- Fixed a configuration bug for the mc test
* Tue Oct 17 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.45-1.
- added submission of mc test with lcgadmin role
- added IPv6 DNS test
* Mon Jul 31 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.44-1.
- updated mc and remotestageout tests to latest version of WMCore
* Mon Jul 24 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.43-1.
- fixed bug in singularity test by which temp dir was not deleted
* Wed Jul 19 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.42-1.
- fixed singularity test, removed glexec test
* Wed May 24 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.41-1.
- added isolation test and removed singularity
- modified tests to run on RHEL7
* Tue Mar 21 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.40-1.
- added test for Singularity
* Tue Feb 28 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.39-1.
- included the latest version of the GFAL2 plugin for WMCore
* Tue Feb 14 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.38-1.
- fixed spacetoken bug in SRM test
* Wed Feb 1 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.37-1.
- new etf_plugin_cms.py to support new VO feed
* Thu Jan 26 2017 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.36-1.
- updated mc test to use the latest xrootd stagout plugin
* Mon Dec 19 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.35-1.
- SRM test now creates dest dir if missing
* Thu Dec 01 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.34-1.
- added -p to gfal-copy for mc test
- removed check that sw area is writable from env test
* Tue Nov 11 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.33-1.
- fixed issue in remotestageout in dealing with file:/// SURLs
* Wed Jul 27 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.32-1.
- modified remotestageout test to use gfal-rm for gsiftp SURLs
- using new GridFTP SE in Nebraska for remotestageout test
* Tue Jul 12 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.31-1.
- added new SE probe
- added dependency from gfal2-python
* Mon Jul 4 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.30-2.
- requires gfal2-all (experimental)
* Mon Jul 4 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.29-2.
- supports site notifications
* Mon Jul 4 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.29-1.
- supports VO feed change to include HTCONDOR-CE
* Thu Jun 9 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.28-1.
- re-added age check of git version of site conf to basic test
* Mon Jun 6 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.27-1.
- basic test uses now gitlab.cern.ch
* Tue May 10 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.26-1.
- new FNAL plugin
* Wed Mar 23 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.25-1.
- xrootd-access critical also for T1s
- remotestageout test now uses gfal-copy
- analysis test uses CMSSW_7_5_2
* Thu Feb 18 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.24-1.
- xrootd-access is now critical for all T2s
* Tue Jan 12 2016 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.23-1.
- xrootd-access: removed US sites from critical sites list
* Tue Dec 15 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.22-1.
- xrootd tests not critical for all sites
* Mon Oct 19 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.21-1.
- Upgraded CMSSW version to 7_4_14 in the xrootd tests
* Mon Sep 21 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.20-1.
- Squid/frontier tests do not give errors if proxy tag missing
* Mon Sep 14 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.19-1.
- More verbose output for the xrootd tests
* Tue Aug 25 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.18-1.
- Fixes to squid and frontier tests
* Thu Aug 6 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.17-1.
- Fixed arguments of doTransfer() in RuntimeSAMRemoteStageOut.py
* Wed Aug 5 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.16-1.
- New WMCore version (updating also the FNAL plugin)
* Fri Jul 17 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.15-1.
- xrootd-fallback does not abort if voms-proxy-init fails
* Tue Jun 23 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.14-1.
- Updated to latest WMCore code and adapted remote stageout test
* Tue Jun 16 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.13-1.
- New version of the remote stageout test which avoids huge outputs
* Wed May 6 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.12-1.
- Removed t1access role for glexec test
* Tue May 5 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.11-1.
- New versions of the xrootd tests
* Mon May 4 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.10-1.
- fetch-from-web-git uses the curl from CRAB3 in CVMFS
* Thu Apr 30 2015 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.9-1.
- New version of the xrootd tests
- Compact version of swinst
- env test prints info on SAM docs
* Thu Nov 13 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.8-1.
- Fixed bug in cms.conf
* Wed Nov 12 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.7-1.
- New GetPfnFromTfc using the senames API
- Change in Condor probe config to test HTCondor CEs
* Fri Nov 7 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.6-1.
- Bug fix in WN-cvmfs
- getPFNfromTFC returns WARNING if SE not an endpoint
* Wed Oct 15 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.5-1.
- New versions of WN-cvmfs and WN-swinst
* Thu Oct 9 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.4-1.
- In xrootd-fallback, added a list of sites which will get critical errors instead of warnings for serious errors
* Mon Sep 29 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.3-1.
- Fixed a typo in cms.conf
- Updated the banner in the swinst test
* Tue Sep 23 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.1.2-1.
- First version using Condor-G for submission
* Mon Sep 22 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.15-1.
- Several improvements to xrootd-fallback
* Tue Sep 16 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.14-1.
- Now xrootd-fallback checks for the number of trivialcatalog lines
* Mon Sep 8 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.13-1.
- Changed xrootd tests to use CMSSW_7_1_7
* Wed Aug 27 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.12-1.
- Fixed uncatched exception in xrootd tests
* Wed Aug 27 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.11-1.
- Small fixes for FNAL in glexec and xrootd tests
- Turned on criticality for xrootd-fallback
- Increased lifetime of payload proxies to 48 hours
- Added check for gfal-copy in CE-cms-env
* Mon May 19 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.10-1.
- Turned on glexec criticality
- New xrootd-acccess test
- Fixed swinst test
* Mon Apr 14 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.9-1.
- Fixed bug in basic test not generating a warning
* Tue Apr 8 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.8-1.
- New plugin for FNAL
* Tue Mar 25 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.7-1.
- Timeouts for job submission spelled out
- Only WARNING if VO_CMS_SW_DIR or OSG_APP do not point to a CVMFS
  mount point
* Tue Mar 4 2014 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.6-1.
- Added fixes to CE-cms-swinst
- Updated WN-cvmfs to the latest version
* Tue Jan 28 2014 Nicolo Magini <Nicolo.Magini@cern.ch> 1.0.5-1.
- Updated WN-cvmfs to the latest version
- Missing tags no longer create warnings in CE-cms-swinst
* Thu Dec 5 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.4-1.
- Updated WN-cvmfs to the latest version
- Added hostname information to CE-cms-basic
* Fri Nov 1 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.3-1.
- Fixed bug in SRM probe
* Tue Oct 29 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.2-1.
- Fixed scheduled timeout to be 23 hours
* Mon Jul 1 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.1-1.
- basic test now uses GIT
* Tue May 15 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 1.0.0-1.
- First version for emi-nagios
* Mon Apr 8 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.55-1.
- Fixed bug in CE-cms-basic affecting path of CVS version of TFC
* Wed Mar 20 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.54-1.
- Updated test_squid.py
* Tue Mar 19 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.53-1.
- "Final" version of remote stageout test
- Free space checks in CE-cms-env generate WARNINGs instead of ERRORs
* Thu Jan 24 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.52-1.
- Fixed xrootd test calculation of stdout size
- Using lsb_version for OS info in env test
* Fri Jan 18 2013 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.51-1.
- Added checks on free space, proxy and CA certs from glidein pilots
* Tue Nov 20 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.50-1.
- Improved CVMFS detection in OSG in swinst test
* Sun Nov 11 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.49-1.
- Fixed syntax error in remotestageout test
* Fri Nov 9 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.48-1.
- Added the remotestageout test
* Thu Nov 1 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.47-1.
- Now the analysis test bypassed the xrootd fallback
- In glprobe.sh, fixed bug in printing payload proxy info
* Mon Oct 29 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.46-1.
- Added cms_param_override
- Added fetch-from-web2
- Updated xrootd tests
- Removed HTML from frontier and squid
* Tue Oct 16 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.45-1.
- New version of the fallback test to allow errors in OSG
- added new architecture in swinst
- Added more information in the output of CE-cms-mc
* Wed Oct 10 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.44-1.
- Fixed output for glprobe.sh
- Fixed syntax error in CE-cms-mc
- HTML-free basic test
- fixed architecture in squid
- testing new architecture in swinst
- added some HTML tags in frontier
* Wed Sep 12 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.43-1.
- Added CE-cms-env test
* Mon Sep 10 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.42-1.
- Adapted tests to new OSG software discovery
- Added xrtood tests
- Nagios configuration adapted to POEM
* Thu Aug 23 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.41-1.
- Fixed bug in mc test causing the test to always pass
- converted errors to warnings in glexec test
* Wed Aug 22 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.40-1.
- mc test now uses WMCore code
* Tue Aug 21 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.39-1.
- Removed SL5 check from analysis and frontier test
* Thu Jul 5 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.38-1.
- Made basic samtest-run compliant and changed SCRAM_ARCH
- Fixed squid exception for RAL in fetch-from-web
* Mon Jul 2 2012 Duncan Ralph <Duncan.Kelley.Ralph@cern.ch> 0.1.37-1.
- Updated swinst test
* Wed Jun 15 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.36-1.
- Changed analysis test to use CMSSW_5_3_1; updated swinst test
* Tue May 15 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.35-1.
- Allow glexec test to succeed wen only the GID changes
* Wed May 9 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.34-1.
- Increased verbosity of Nagios on WN to 3
- Removed calls to unwanted commands in glprobe.sh; added sleep in analysis test to delay its execution
* Wed Apr 25 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.33-1.
- Fixed bug in RPM (Hash_local.pm and cms_glexec being links)
* Thu Apr 16 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.32-1.
- Set --timeout-job-discard to 84600 and --timeout-wnjob-global to 1800 for JobState; updated glexec probe
* Wed Mar 14 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.31-1.
- Updated basic, CMSSW_frontier.sh and swinst
* Tue Feb 14 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.30-1.
- Updated swinst
* Thu Jan 26 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.29-1.
- Fixed bug in Hash_local.pm
* Wed Jan 18 2012 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.28-1.
- All WN tests (apart mc) now generate a good HTML
* Wed Dec 14 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.27-1.
- Removed hr.srce.SRM2-CertLifetime from the tests to run
* Wed Nov 30 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.26-1.
- Removed slc4 architecture from swinst test
* Wed Nov 16 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.25-1.
- Changed mc test to support fallback
* Wed Nov 2 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.24-1.
- Added support for ARC-CE in Hash_local.pm
- New version of swinst
- NCG cron disabled by default
* Tue Oct 4 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.23-1.
- NCG configuration supports OSG services
* Tue Sep 27 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.22-1.
- Fixed bug in SRM probe causing tests to fail when a cached PFN is used that was not properly deleted by a previous test
* Tue Sep 13 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.21-1.
- Fixed small bug in analysis test output
* Tue Sep 13 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.20-1.
- Updated analysis, swinst tests to new CMSSW version
* Fri Jul 29 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.19-1.
- Fixed bug in new Frontier test
* Wed Jul 27 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.18-1.
- Updated Frontier test
- Added timeouts to lcg-del in mc test
* Mon Jun 22 2011  Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.17-1.
- Added new glexec test
- New SRM probe with correct behaviour for the GetPFNFromTFC test
- Modified Hash_local.pm to expose org.cms.glexec.WN-gLExec for CREAMCE
* Tue May 31 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.16-1.
- Added glexec test
* Tue May 24 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.15-1.
- Added white/black list of sites
* Fri May 6 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.13-1.
- Timeouts for jobs lengthened to 12 h
- Timeouts for SRM lengthened to 30 minutes
* Tue Apr 11 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.12-1.
- Updated tests.
* Tue Mar 29 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.11-1.
- Updated tests, increased SRMAll timeout, fixed missing WN conf.
* Thu Mar 24 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.10-1.
- Updated tests and configuration to run SRM tests only once.
* Mon Mar 14 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.9-1.
-  Updated tests.
* Mon Feb 21 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.8-1.
-  Added configuration files.
* Fri Feb 18 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.7-1.
-  Added configuration files.
* Fri Feb 18 2011 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.6-1.
-  Added new version of srmvometrics.py.
* Thu Dec 9 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.5-1.
-  Added new version of srmvometrics.py.
* Tue Nov 9 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.4-1.
-  Added custom variables to sam-generic-wn in services.cfg.
* Wed Aug 25 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.3-1.
-  Upgraded to latest version of SRM probe.
* Tue Aug 3 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.2-1.
-  Fixed bug in SRM probe.
* Fri Jul 23 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.1-1.
-  Updated the CMS tests and included them all, including SRM.
* Mon Feb 08 2010 Andrea Sciaba <Andrea.Sciaba@cern.ch> 0.1.0-1.
-  First version with some WN tests.
