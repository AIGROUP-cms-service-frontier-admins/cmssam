#!/usr/bin/env python
##############################################################################
#
# NAME:        nagtest-run
#
# FACILITY:    SAM (Service Availability Monitoring)
#
# COPYRIGHT:
#         Copyright (c) 2009, Members of the EGEE Collaboration.
#         http://www.eu-egee.org/partners/
#         Licensed under the Apache License, Version 2.0.
#         http://www.apache.org/licenses/LICENSE-2.0
#         This software is provided "as is", without warranties
#         or conditions of any kind, either express or implied.
#
# DESCRIPTION:
#
#         Wrapper script for "semi"-Nagios checks - ie. tests that return
#         Nagios compliant exit codes, but summary and details data output is
#         not Nagios compliant.
#
# AUTHORS:     Konstantin Skaburskas, CERN
#
# CREATED:     24-Feb-2009
#
# NOTES:
#
# MODIFIED:
#
##############################################################################

"""
wrapper script for "semi"-Nagios checks.

Wrapper script for "semi"-Nagios checks - ie. tests that return
Nagios compliant exit codes, but summary and details data output is
not Nagios compliant.

Konstantin Skaburskas <konstantin.skaburskas@cern.ch>, CERN
SAM (Service Availability Monitoring)
"""

import os
import sys
import getopt
import signal
import re

version = '0.9'

# global definitions
testfullpath  = None # -m
hostname      = None # -H
_nopass_H     = False # --nopass-H
uri           = None # -u
service       = None # -s
timeout_test  = 600  # -t
envvars       = {} # -e
VO            = 'ops' # --vo
verbose       = 0 # -v
nag_work = '/var/lib/gridprobes/%s/nag' # -w
testargs = '' # -o
# order: -x, X509_USER_PROXY, /tmp/x509up_u${UID}
if os.environ.has_key('X509_USER_PROXY'):
    proxy = os.environ['X509_USER_PROXY']
else:
    proxy = '/tmp/x509up_u'+str(os.geteuid())
nag_rcs = {'OK'      :0,
          'WARNING'  :1,
          'CRITICAL' :2,
          'UNKNOWN'  :3}
process = None

usage_short = """Usage:
%s -m <pathToTest> -H <hostname> | -u <uri> [-s <service>]
[-t|--timeout sec] [-V] [-e <env,..>] [-h|--help] [--vo <VO>]
[-x proxy] [-w <path>] [-v <0-3>] [-o "test options"]
"""%(os.path.basename(sys.argv[0]))

usage_long = """   Mandatory parameters:
-m <pathToTest>    Test specified by an absolute path.
-H <hostname>      Hostname to test. Passed to test.
-u <uri>           Service endpoint URI. Passed to test as -u.
   Optional parameters:
-s <service>       Name of a service to be tested.
-h|--help          Displays help
-t|--timeout sec   Sets test's global timeout. Passed to test as (-t).
                   (Default: %i)
--vo <VO>          VO name to set as NAG_VO environment variable.
                   (Default: %s)
-v <0-3>           Verbosity level. Passed to the test. (Default: %i)
-e <env,..>        Comma delimited list of KEY=value environment variables that
                   are required to be exported before launching the test.
-w <path>          Working directory for checks.
                   (Default: %s)
-x                 VOMS proxy (Order: X509_USER_PROXY, /tmp/x509up_u<UID>, -x)
-o "test options"  Options to be passed to the test.
-V                 Displays version.

You must specify test (-m), and hostname (-H) or uri (-u).

The wrapper script runs a test script available as an executable <pathToTest>.

Arguments given with -o option are passed to the test script.

The script captures (in line buffered mode) stdout and stderr of the test
script and produces Nagios compliant output consisting of
- test status (on the first line)
- multi-line details data
The script assumes that test it runs returns 0-3. If return code is greater
than 3 it issues WARNING instead.
"""%(timeout_test,
     VO,
     verbose,
     nag_work%'<VO>')

usage = usage_short + '\n' + usage_long

def to_status(code):
    """
    Return Nagios status as string.
    """
    if code in (0, 'OK', 'ok', 'Ok'):
        return 'OK'
    elif code in (1, 'WARNING', 'warning', 'Warning'):
        return 'WARNING'
    elif code in (2, 'CRITICAL', 'critical', 'Critical'):
        return 'CRITICAL'
    else:
        return 'UNKNOWN'

def check_opts(opts):
    """Command line options sanity check.
    """

    def _exit(rc, msg):
        sys.stdout.write(usage_short)
        sys.stdout.write(msg)
        sys.exit(rc)

    rc = nag_rcs['UNKNOWN']

    if len(opts) == 0:
        _exit(rc, """
    Provide test with mandatory parameters
 -m <pathToTest> -H <hostname> | -u <uri>
    or
 -h for help\n""")

    k = [x[0] for x in opts]
    if '-h' in k or '--help' in k:
        return
    if not '-m' in k:
        _exit(rc, '\nERROR: mandatory parameter missing: -m <pathToTest>\n')
    if (not '-H' in k) and (not '-u' in k):
        _exit(rc, '\nERROR: mandatory parameter missing: -H <hostname> | -u <uri>\n')
        sys.exit(rc)

def parse_args(argv):
    "- argv - sys.argv"

    global testfullpath, service, timeout_test, VO, nag_work, \
        proxy, testargs, hostname, envvars, _nopass_H, verbose, uri

    opts, args = (None, None)
    try:
        opts, args = getopt.getopt(argv[1:],'Vhm:t:v:x:o:w:s:H:e:u:',
                                ['help','timeout=','vo=','nopass-H'])
    except getopt.GetoptError, e:
        sys.stdout.write(usage)
        sys.stdout.write("Error : %s\n"% e)
        sys.exit(0)

    # command line options sanity check
    check_opts(opts)

    for o,v in opts:
        if o in ('-V'):
            sys.stdout.write(version+'\n')
            sys.exit(0)
        elif o in ('-h','--help'):
            sys.stdout.write(usage)
            sys.exit(0)
        elif o == '-m':
            if not os.path.isabs(v):
                status = 'UNKNOWN'
                sys.stdout.write('%s: -m must be an absolute path.\n'%status)
                sys.exit(nag_rcs[status])
            testfullpath = v
        elif o == '-s':
            service = v
        elif o in ('-t','--timeout'):
            timeout_test = int(v)
        elif o == '--vo':
            VO = v
        elif o == '-v':
            verbose = v
        elif o == '-x':
            proxy = v
        elif o == '-o':
            testargs = v
        elif o == '-w':
            nag_work = v
        elif o == '-H':
            hostname = v
        elif o == '-e':
            for s in v.split(','):
                if not s:
                    continue
                try:
                    a,b = s.split('=')
                    if not a:
                        raise ValueError
                except ValueError:
                    _exit(nag_rcs['UNKNOWN'],
                          'ERROR: badly provided env. variable: %s\n' % s)
                if not b:
                    _exit(nag_rcs['UNKNOWN'],
                          'ERROR: no value provided for env. variable: %s\n' % a)
                else:
                    envvars[a] = b
        elif o == '--nopass-H':
            _nopass_H = True
        elif o == '-u':
            uri = v
        else:
            pass
    os.environ['X509_USER_PROXY'] = proxy

def set_env_cliparams():
    ""
    global hostname, timeout_test, uri, verbose

    if timeout_test:
        os.environ['NAG_TIMEOUT'] = str(timeout_test)
    if hostname:
        os.environ['NAG_HOSTNAME'] = hostname
    if uri:
        os.environ['NAG_URI'] = uri
    if verbose:
        os.environ['NAG_VERBOSE'] = str(verbose)

def set_env():
    "Set SNAG environment"
    global nag_work

    os.environ['X509_USER_PROXY'] = proxy
    os.environ['NAG_VO'] = VO
    for status in nag_rcs.keys():
        os.environ['NAG_'+status] = str(nag_rcs[status])
    try:
        nag_work = nag_work % VO
    except TypeError:
        pass
    # /var/lib/gridmonsam/$SAME_VO/nag/
    if not os.environ.has_key('NAG_WORK'):
        os.environ['NAG_WORK'] = nag_work
    if not os.environ.has_key('NAG_CHECK_DIRNAME'):
        os.environ['NAG_CHECK_DIRNAME'] = os.path.dirname(testfullpath)
    if service:
        if not os.environ.has_key('NAG_SERVICE_NAME'):
            os.environ['NAG_SERVICE_NAME'] = service
        # /var/lib/gridmonsam/$SAME_VO/nag/$NAG_SERVICE_NAME
        if not os.environ.has_key('NAG_SERVICE_WORK'):
            os.environ['NAG_SERVICE_WORK'] = nag_work+'/'+service
        # /var/lib/gridmonsam/$SAME_VO/nag/$NAG_SERVICE_NAME/nodes/<hostname>
        if not os.environ.has_key('NAG_CHECK_WORK'):
            os.environ['NAG_CHECK_WORK'] = nag_work+'/'+service+'/nodes/'+hostname
        for _dir in ['NAG_WORK', 'NAG_SERVICE_WORK', 'NAG_CHECK_WORK']:
            try:
                if not os.path.isdir(os.environ[_dir]):
                    os.makedirs(os.environ[_dir])
            except OSError, e:
                if re.search('File exists', str(e)):
                    pass
                else:
                    status = 'UNKNOWN'
                    stsmsg = detmsg = status+": OSError: "+str(e)+'\n'
                    sys.stdout.write(stsmsg)
                    sys.stdout.write(detmsg)
                    sys.exit(nag_rcs[status])

    for k,v in envvars.items():
        os.environ[k] = v

def spawn_pexpect(cmd):
    """Use Pexpect to spawn a process.
    Line-buffered pipes from/to child.
    """
    global process

    try:
        from gridmon.process.pexpectpgrp import SpawnPgrp
        from gridmon.process.pexpect import ExceptionPexpect, EOF, TIMEOUT
    except ImportError, e:
        sys.stdout.write('ERROR: %s\n' % e)
        sys.exit(nag_rcs['UNKNOWN'])

    read_timeout = 30 # default value in Pexpect is 30 sec
    process = SpawnPgrp(cmd, timeout=read_timeout)

    process.alrm_timeout = process.sigterm = None

    l       = True
    lines   = []
    status  = None
    rc = None

    while l:
        try:
            l = process.readline()
        except TIMEOUT, e:
            if not process.isalive():
                lines+="\nTimed out after %.2f sec while waiting for stdout from child.\n"%\
                        (float(read_timeout))
                lines+="Child process(es) died.\n"
                break
        if not l:
            break
        else:
            lines.append(l)

    # Hack. Othervise obtaining of exit status and return code
    # of the child process doesn't work properly.
    if process.isalive():
        pass

    for i in range(len(lines)):
        lines[i] = '\n'.join(reduce(lambda x,y: x[:-1]+[y[0]+x[-1][len(y[0]):]]+y[1:], \
                                  [str(x).split('\n') for x in str(lines[i]).split('\r')]))

    # As we could not get any exception (e.g., IOError) from Pexpect
    # in case if child was sent a signal - lets rely on our variable
    # set in signal handler. Otherwise, collect return code (exit status)
    # and status from normally exited process.
    if process.alrm_timeout:
        if process.sigterm: # SIGTERM was sent
            lines += "\nCaught SIGTERM while executing test! (test timeout %d)"% \
                (timeout_test), "summary: SIGTERM caught"
            rc = nag_rcs['WARNING']
        else: # test timeout
            lines += "\nTimeout while executing test after %d seconds!"% \
                (timeout_test), "summary: test timeout"
            rc = nag_rcs['CRITICAL']
    else:
        try:
            pid, status = os.waitpid(process.pid,os.WNOHANG)
        except OSError:
            status = process.status
        # exitstatus = os.WEXITSTATUS(status)
        # 'return code of application' == 'exit status'
        #rc=process.exitstatus
        rc = os.WEXITSTATUS(status)

    return (rc, lines)

def spawn_popen(cmd):
    """Use popen to spawn a process.
    Block-buffered pipes from/to child.
    """
    from gridmon.process.popenpgrp import Popenpgrp

    process = Popenpgrp(cmd)

    l      = True
    lines  = []
    status = None
    rc     = None

    while l:
        try:
            l = process.fromchild.readline()
        except IOError:
            lines += process.fromchild.readlines()
            lines += "Timeout when executing test after %d seconds!"%\
                        (timeout),"summary: timeout"
            l = None
            rc = nag_rcs['WARNING']
        if l:
            lines.append(l)
    if not rc:
        status = process.poll()
        while status<0:
            process.wait()
            status = process.poll()
        rc = os.WEXITSTATUS(status)

    return (rc, lines)

def sig_alrm(sig, stack):
    "SIGALARM handler."
    global process
    if process.isalive():
        os.kill(-process.pid, sig)
        process.alrm_timeout = True

def sig_term(sig, stack):
    "SIGTERM handler."
    global process
    sig_alrm(signal.SIGALRM, stack)
    process.sigterm = True

def outputsanitiser(str):
    'Apply string substitutions to make our schedulers happy.'
    patterns = {
                # Nagios treats data after pipes as performance data
                '\|\||\|' : 'OR'
                }
    for p,s in patterns.items():
        str = re.sub(p, s, str)
    return str

# main block
if __name__ == '__main__':
    parse_args(sys.argv)
    set_env()
    set_env_cliparams()

    # command to execute
    h = ''
    if hostname and not _nopass_H:
        h = '-H '+hostname
    elif hostname:
        h = hostname
    cmd = '%(p)s %(h)s %(t)s %(v)s %(o)s' % {
                        'p':testfullpath,
                        'h':h,
                        't':'-t '+str(timeout_test),
                        'v':'-v '+str(verbose),
                        'o':testargs.replace("'","")}
    if uri:
        cmd += ' -u '+uri

    signal.signal(signal.SIGTERM, sig_term)
    signal.signal(signal.SIGALRM, sig_alrm)
    signal.alarm(timeout_test)

    # Try with Pexpect first. If it fails - fall-back to popen().
    try:
        from gridmon.process.pexpect import ExceptionPexpect
        rc, lines = spawn_pexpect(cmd)
    except ExceptionPexpect:
        rc, lines = spawn_popen(cmd)

    summary = None
    if lines and lines[-1].find("summary: ")==0:
        summary = lines[-1][9:].strip('\n')[:255]
        lines.pop()
#    else:
#        summary = lines[-1][:255]
#        lines.pop()
    if not rc in nag_rcs.values():
        lines += "WARNING: Unknown test return code: %d\n" % rc
        rc = nag_rcs['UNKNOWN']

    status = to_status(rc)
    if summary:
        summary = '%s: %s' % (status, summary)
    else:
        summary = status

    lines = outputsanitiser(''.join(lines).strip('\n'))
    summary = outputsanitiser(summary)

    sys.stdout.write(summary+'\n')
    sys.stdout.write(lines+'\n')
    sys.exit(rc)
