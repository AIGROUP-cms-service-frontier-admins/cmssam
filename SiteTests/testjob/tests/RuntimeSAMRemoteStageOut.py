#!/usr/bin/env python2.7
"""
_RuntimeSAMRemoteStageOut_

Test script to determine remote stage out to reference storage elements
is working properly at a site

"""

import time
import os
import sys
import random
import logging

from WMCore.Storage.Registry import retrieveStageOutImpl

import WMCore.Storage.Backends
import WMCore.Storage.Plugins

class StageOutDiagnostic:
    """
    _StageOutDiagnostic_

    Object to test the remote stage out details step by step


    """
    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.summary = {}
        self.summary.setdefault('RemoteStageOut' , "NotRun")
        self.summary.setdefault('CleanUp' , "NotRun")
        self.copySuccessful = False
        self.cleanupSuccessful = False
        self.status = 0
        # Force srmv2 protocol and lcg-cp
        self.command = 'gfal2'
        self.protocol = 'srmv2'   # dummy parameter for gfal2
        self.options = None
        self.datestamp = time.asctime(time.localtime(time.time()))
        self.datestamp = self.datestamp.replace(" ", "-").replace(":", "_")
        self.testLFN = "/store/user/sam/SAM/RemoteStageOutTest-%s-%s" % (os.popen("hostid").read().strip(), self.datestamp)
        self.testLFNprefixes = [ "gsiftp://eoscmsftp.cern.ch//eos/cms", \
                                 "srm://stormfe1.pi.infn.it:8444/srm/managerv2?SFN=/cms", \
                                 "gsiftp://red-gridftp.unl.edu//mnt/hadoop/user/uscms01/pnfs/unl.edu/data4/cms" ]
        random.shuffle(self.testLFNprefixes)
        

    def __call__(self):
        """
        _operator()_

        Invoke step by step tests and create the summary

        """
        try:
            self.testRemoteStageOut()
        except Exception as ex:
            print str(ex)

        if self.copySuccessful==True and self.cleanupSuccessful==True:
            self.status=0
        elif self.copySuccessful==True and self.cleanupSuccessful==False:
            self.status=2
        else:
            self.status=1

        self.complete()
        
        

    def complete(self):
        """
        _complete_

        Print summary after tests
        
        """
        msg = "==== StageOut Test Summary ====\n"
        if self.status == 1:
            msg += "Status: FAILED: %s\n" % self.status
        else:
            msg += "Test Successful\n"
            
        for key, val in self.summary.items():
            msg += "  Test: %s : %s\n" % (key, val)
        print msg
        return
        
        
    def testRemoteStageOut(self):
        """
        _testRemoteStageOut_

        Test remote stage out

        """

        msg = ""

        handle = open("TEST-FILE", 'w')
        for i in range(0, 1000):
            handle.write("This is a test file\n")
        handle.close()
        sourcePFN = os.path.join(os.getcwd(), "TEST-FILE")
                
        try:
            impl = retrieveStageOutImpl(self.command)
        except Exception as ex:
            msg += "Unable to retrieve impl for remote stage out:\n"
            msg += "Error retrieving StageOutImpl for command named: %s\n" % (
                self.command,)
            self.summary['RemoteStageOut'] = \
                                          "Failure: Can't retrieve StageOut Impl"
            raise RuntimeError, msg

        self.summary['RemoteStageOut'] = ""
        self.summary['CleanUp'] = ""
        # Try remote stageout to all target PFNs in the list
        for lfnPrefix in self.testLFNprefixes:
            targetPFN = lfnPrefix + self.testLFN

            print "Staging out to %s\n" \
                  % targetPFN
            
            try:
                impl.retryPause = 15
                impl.numRetries = 1
                impl(self.protocol, sourcePFN, targetPFN)
                self.copySuccessful = True
                self.summary['RemoteStageOut'] += \
                                          "Success: Remote Stage Out to %s Succeeded\n" \
                                          % targetPFN
            except Exception as ex:
                self.copySuccessful = False
                msg += "Failure for remote stage out to %s :\n" \
                       % targetPFN
                msg += str(ex)
                self.summary['RemoteStageOut'] += \
                                          "Failure: Remote Stage Out to %s Failed\n" \
                                          % targetPFN
            print "Cleaning up remote file %s\n" \
                  % targetPFN

            # Clean up the file
            try:
                impl.removeFile(targetPFN)
                self.cleanupSuccessful = True
                self.summary['CleanUp'] += "Success: Cleanup operation of %s Succeeded\n" \
                                           % targetPFN
            except Exception as ex:
                self.cleanupSuccessful = False
                msg += "Error performing Cleanup command "
                msg += "On PFN: %s\n" % targetPFN
                msg += str(ex)
                self.summary['CleanUp'] += "Failure: Cleanup operation of %s Failed\n" \
                                           % targetPFN
            
            if self.copySuccessful:
                return
            
        # All remote PFNs failed, raise error
        raise RuntimeError, msg
                    

if __name__ == '__main__':

    diagnostic = StageOutDiagnostic()

    diagnostic()
    sys.exit(diagnostic.status)
