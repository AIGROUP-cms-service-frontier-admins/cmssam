#!/usr/bin/env python
"""
_FwkJobRep_

Python object support and parsers for generating/manipulating Framework
Job Reports.

Runtime Safe.


"""
__version__ = "$Revision: 1.2 $"
__revision__ = "$Id: __init__.py,v 1.2 2007/03/12 10:13:28 asciaba Exp $"
__author__ = "evansde@fnal.gov"
__all__ = []



