#!/usr/bin/env python
"""
_StageOut_

Runtime framework for managing stage out of files based on a named
plugin system that loads a stage out implementation based on the
command name provided in the Site Config File

"""
__all__ = []
__revsion__ = "$Id: __init__.py,v 1.3 2008/01/30 10:20:24 asciaba Exp $"
__version__ = "$Revision: 1.3 $"
__author__ = "evansde@fnal.gov"

