#!/bin/bash

shopt -s expand_aliases

SCRIPT_REVISION="2020-03-20, Dave Dykstra and Edita Kizinevic"

# Script does not use backupproxyurl if it is SAM test, but script uses backupproxyurl if it is pilot
includebackups=false
while getopts ":b" opt; do
  case ${opt} in
    b)
      includebackups=true
      ;;
    \?)
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
    esac
done

# Source the CMS environment
if [ -n "$OSG_GRID" ] ; then
    [ -f $OSG_GRID/setup.sh ] && source $OSG_GRID/setup.sh
    if [ -d $OSG_APP/cmssoft/cms ] ; then
        SW_DIR=$OSG_APP/cmssoft/cms
    elif [ -d $CVMFS/cms.cern.ch ] ; then
        SW_DIR=$CVMFS/cms.cern.ch
    elif [ -d /cvmfs/cms.cern.ch ] ; then
        SW_DIR=/cvmfs/cms.cern.ch
    else
        echo "ERROR: Cannot find CMS software in OSG node"
        echo "summary: SW_DIR_UNDEF"
        exit $SAME_ERROR
    fi
elif [ -n "$VO_CMS_SW_DIR" ] ; then
    SW_DIR=$VO_CMS_SW_DIR
else
    SW_DIR=/cvmfs/cms.cern.ch
fi
tmpfile=`mktemp /tmp/tmp.XXXXXXXXXX`
source $SW_DIR/cmsset_default.sh > $tmpfile 2>&1
result=$?
grep 'Your shell is not able to find' $tmpfile > /dev/null
result2=$?
if [ $result != 0 -o $result2 == 0 ] ; then
    cat $tmpfile
    rm -f $tmpfile
    echo "ERROR: CMS software initialisation script cmsset_default.sh failed"
    echo "summary: NO_SETUP_SCRIPT"
    exit $SAME_ERROR
fi
rm -f $tmpfile

# Print out node name
node=`hostname`
echo "node: $node"
echo

# Check that environmental variable SAME_OK is set
if [ -z "$SAME_OK" ] ; then
    echo "ERROR: SAME_OK not defined"
    exit 1
fi

# Check that environmental variable SAME_ERROR is set
if [ -z "$SAME_ERROR" ] ; then
    echo "ERROR: SAME_ERROR not defined"
    exit 1
fi

# Check that environmental variable SAME_WARNING is set
if [ -z "$SAME_WARNING" ] ; then
    echo "ERROR: SAME_WARNING not defined"
    exit 1
fi

# Check that environmental variable CMS_PATH is set
if [ -z "$CMS_PATH" ] ; then
    echo "ERROR: CMS_PATH not defined"
    exit 1
fi

# Print script version information
echo "script version: $SCRIPT_REVISION"
echo

# Get site name
site_local_config_file=${CMS_PATH}/SITECONF/local/JobConfig/site-local-config.xml
if [ ! -f "$site_local_config_file" ] ; then
    echo "ERROR: file ${site_local_config_file} does not exist"
    exit $SAME_ERROR
fi
site=`grep -oP '(?<=site name=").*(?=")' $site_local_config_file`

# Create Working Directory
mkdir squid
cd squid
current=`pwd`
echo "Current directory is: ${current}"
echo

# Script checks failover if it is SAM test, but script does not check failover if it is pilot
failover=false
if ! $includebackups; then
    if [ ! -z "$site" ] ; then
        site_summary_file=site_summary.txt
        url=http://wlcg-squid-monitor.cern.ch/failover/failoverCMS/$site_summary_file
        wget -q -O $site_summary_file $url
        if [ -f "$site_summary_file" ] ; then
            result="`grep -P "$site[, \t]" $site_summary_file`"
            if [ ! -z "$result"  ] ; then
                failover=true
            fi
        else
            echo "$site_summary_file file is not retrieved from $url"
        fi
    fi
fi

# Set up CMSSW
echo "Set up CMSSW ... starting"
CMSREL="`scram l | grep -B 1 "cms.cern.ch" | grep " CMSSW" | tail -1 | awk '{print $2}'`"
scram p $CMSREL
if [ $? -ne 0 ] ; then
    echo "ERROR: $CMSREL not available"
    exit $SAME_WARNING
fi
echo "Set up of $CMSREL completed"
echo
cd $CMSREL/src
cmsenv

# Function to check proxies
# $1 - list of proxies
# $2 - $includebackups
check_proxies() {
    if $2; then
        backupproxyurl="*(backupproxyurl=*"
    else
        backupproxyurl="NONE"
    fi
    for PART in $1; do
        proxy=`echo $PART | sed 's/^.*(/(/'`
        case $proxy in
            *"(proxyurl="* | $backupproxyurl)
                found_proxy=true
                hostname=${proxy#*//}
                hostname=${hostname%:*}
                # The frontier client does not work with IPv6 addresses in URLs for now
                IP_addresses=`getent ahostsv4 $hostname | awk '/STREAM/{print $1}'`
                if [ -z "$IP_addresses" ]; then
                    IP_addresses=$hostname
                elif getent ahostsv6 $hostname | awk '/STREAM/{print $1}' | grep -qv ^::ffff:; then
                    IP_addresses="$IP_addresses $hostname"
                fi
                for IP_address in $IP_addresses; do
                    proxy_IP=`echo $proxy | sed "s/$hostname/$IP_address/g"`
                    echo "Query $proxy started:" `date`
                    echo 'select 1 from dual' | FRONTIER_SERVER="$proxy_IP(serverurl=http://cmsfrontier.cern.ch:8000/FrontierProd)(failovertoserver=no)" fn-req > $output
                    if grep -q " 1 NUMBER" $output ; then
                        echo "$proxy_IP is OK"
                        ever_succeeded=true
                    else
                        echo "$proxy_IP is FAILED:"
                        cat $output
                        ever_failed=true
                    fi
                    echo "Query $proxy ended:" `date`
                    echo
                done
                ;;
            *"(proxyconfigurl="*)
                if [ "$found_proxyconfig" == false ]; then
                    proxy_config_url=$proxy
                    echo "Converting $proxy_config_url proxyconfigurl to proxyurl/backupproxyurl started:" `date`
                    echo
                    echo 'select 1 from dual' | FRONTIER_LOG_LEVEL=debug FRONTIER_SERVER="$proxy(serverurl=http://cmsfrontier.cern.ch:8000/FrontierProd)(failovertoserver=no)" fn-req > $debug_output 2>&1
                    if grep -q "FindProxyForURL" $debug_output ; then
                        found_proxyconfig=true
                        PROXYLIST="`sed -n '/returned "PROXY/{s/.*returned "//;s/PROXY /(proxyurl=/g;s/;/)/g;s/"/)/;p;q}' $debug_output`"
                        PROXYLIST=`echo $PROXYLIST | sed "s/(proxyurl=http:\/\/cmsbpfrontier.cern.ch/(backupproxyurl=http:\/\/cmsbpfrontier.cern.ch/g"`
                        PROXYLIST=`echo $PROXYLIST | sed "s/(proxyurl=http:\/\/cmsbproxy.fnal.gov/(backupproxyurl=http:\/\/cmsbproxy.fnal.gov/g"`
                        check_proxies "$PROXYLIST" $2
                    fi
                    echo "Converting $proxy_config_url proxyconfigurl to proxyurl/backupproxyurl ended:" `date`
                    echo
                fi
                ;;
        esac
    done
}

# Print out cmsGetFnConnect frontier://FrontierProd
FNCONNECT="`cmsGetFnConnect frontier://FrontierProd | sed 's/)/) /g'`"
echo "Contents of cmsGetFnConnect frontier://FrontierProd are:"
echo "$FNCONNECT" | tr " " "\n"
echo

# Check proxies
debug_output="debug_output.txt"
output="output.txt"
ever_failed=false
ever_succeeded=false
found_proxyconfig=false
found_proxy=false
check_proxies "$FNCONNECT" $includebackups

#Check test results
if [ "$failover" == true ]; then
    message=$'many database queries from the site have connected directly to the Frontier servers or backup proxies, with a high rate of queries not going through the local squid(s):\n'$result
    if [ "$ever_failed" == false ] && [ "$ever_succeeded" == true ]; then
        echo "Proxy test is OK, however $message"
        exit $SAME_WARNING
    elif [ "$ever_failed" == true ] && [ "$ever_succeeded" == true ]; then
        echo "At least one of proxies is FAILED and $message"
        exit $SAME_WARNING
    elif [ "$found_proxy" == false ]; then
        echo "No proxies are found and $message"
        exit $SAME_WARNING
    else
        echo "Proxy test is FAILED and $message"
        exit $SAME_ERROR
    fi
else
    if [ "$ever_failed" == false ] && [ "$ever_succeeded" == true ]; then
        echo "Proxy test is OK."
        exit $SAME_OK
    elif [ "$ever_failed" == true ] && [ "$ever_succeeded" == true ]; then
        echo "At least one of proxies is FAILED."
        exit $SAME_WARNING
    elif [ "$found_proxy" == false ]; then
        echo "No proxies are found."
        exit $SAME_OK
    else
        echo "Proxy test is FAILED."
        exit $SAME_ERROR
    fi
fi
