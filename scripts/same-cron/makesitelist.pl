#!/usr/bin/perl
#
# Script to modify the SAM configuration to use the list of CMS sites taken from the WWW
#
# Usage: ./makesitelist.pl template sam_conf
#

$template = $ARGV[0];
$confile = $ARGV[1];
$sameworkdir = $ARGV[2];

@sites = (
          'https://cern.ch/asciaba/SitesList/CMS-T0.txt',
          'https://cern.ch/asciaba/SitesList/CMS-T1.txt',
          'https://cern.ch/asciaba/SitesList/CMS-T2.txt',
          'https://cern.ch/asciaba/SitesList/CMS-T3.txt'
         );

$sites = join ' ', @sites;

$ret = system("wget --tries=1 --timeout=300 $sites -O /tmp/sitelist");
if ( $ret ) {
  die "makesitelist.pl: error downloading site lists\n";
}

$conf = "sitename=";
open(FILE, "cat /tmp/sitelist | dos2unix |") or die "makesitelist.pl: cannot read temporary file\n";
while (<FILE>) {
  chomp;
  next if /^\#/;
  next unless /\w+/;
  $conf .= "$_,";
}
close FILE;
unlink('/tmp/sitelist');
$conf =~ s/,$//;
open(TEMPLATE, "$template") or die "makesitelist.pl: Cannot open $template\n";
open(CONF, "> $confile") or die "makesitelist.pl: Cannot open $confile\n";
while (<TEMPLATE>) {
  chomp;
  if ( /^common_filter/ ) {
    s/\"$//;
    $_ .= " $conf\"";
  }
  if ( defined $sameworkdir ) {
    if ( /^workdir/ ) {
      s/same/$sameworkdir/;
    }
  }
  print CONF "$_\n";
}
close TEMPLATE;
close CONF;
