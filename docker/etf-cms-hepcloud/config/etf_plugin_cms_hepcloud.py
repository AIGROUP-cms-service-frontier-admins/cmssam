import logging

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

XROOT_METRICS = (
    'org.cms.SE-xrootd-contain',
    'org.cms.SE-xrootd-connection',
    'org.cms.SE-xrootd-version',
)


def run(url, ipv6=False):
    log.info("Processing vo feed: %s" % url)

    h = Hosts()
    h.add('storm.mib.infn.it', tags=["XROOTD", ])
    h.serialize()

    c = Checks()
    c.add_all(XROOT_METRICS, tags=["XROOTD"])
    c.add("org.cms.SE-xrootd-read", hosts=('storm.mib.infn.it',),
          params={'args': {'--site': 'T3_IT_MIB',
                           '--endpoint': 'storm.mib.infn.it:1094',
                           '-4': ''}, '_tags': 'XROOTD'})
    c.serialize()
