#!/bin/bash
set -e

_term() {
  omd stop
  if [ -f /var/run/crond.pid ]; then
    kill -9 `cat /var/run/crond.pid`
    rm -f /var/run/crond.pid
  fi
  rm -rf /opt/omd/sites/etf/etc/nagios/conf.d/wlcg/
}

trap _term SIGINT SIGTERM

cat << "EOF"
 _____ _____ _____    ____ __  __ ____
| ____|_   _|  ___|  / ___|  \/  / ___|
|  _|   | | | |_    | |   | |\/| \___ \
| |___  | | |  _|   | |___| |  | |___) |
|_____| |_| |_|      \____|_|  |_|____/
========================================
EOF
ncgx_version=`rpm -q --qf "%{VERSION}-%{RELEASE}" ncgx`
echo "ETF version: ${ncgx_version} Copyright CERN 2016"
echo "License: https://gitlab.cern.ch/etf/ncgx/blob/master/LICENSE"
echo "Check_MK version: $CHECK_MK_VERSION"
echo "Copyright by Mathias Kettner (https://mathias-kettner.de/check_mk.html)"
plugins=`rpm -qa | grep nagios-plugins`
echo "Plugins:" 
echo "${plugins}"
echo ""
echo "Starting xinetd ..."
export XINETD_LANG="en_US" && /usr/sbin/xinetd -stayalive -pidfile /var/run/xinetd.pid
if [[ -n $CHECK_MK_USER_ID ]] ; then
   echo "Changing $CHECK_MK_SITE uid to $CHECK_MK_USER_ID"
   /usr/sbin/usermod -u $CHECK_MK_USER_ID $CHECK_MK_SITE
   chown -R $CHECK_MK_SITE /etc/ncgx /var/cache/ncgx /var/cache/nap
   chown -R $CHECK_MK_SITE /usr/libexec/grid-monitoring/probes/
fi
if [[ -n $CHECK_MK_GROUP_ID ]] ; then
   echo "Creating group with gid $CHECK_MK_GROUP_ID"
   /usr/sbin/groupadd -g $CHECK_MK_GROUP_ID sec
   /usr/sbin/groupmems -g sec -a $CHECK_MK_SITE
fi

echo "Starting crond ..."
/usr/sbin/crond -m off -p -s

echo "Copying certificates ..."
if [ ! -f /etc/grid-security/hostcert.pem ]; then
    echo "Failed to find certificates in /etc/grid-security"
    exit
fi
mkdir -p /opt/omd/sites/etf/etc/nagios/globus/
cp /etc/grid-security/host*.pem /opt/omd/sites/etf/etc/nagios/globus/
cp /etc/grid-security/etf_srv*.pem /opt/omd/sites/etf/etc/nagios/globus/
chown -R ${CHECK_MK_SITE}.${CHECK_MK_SITE} /opt/omd/sites/etf/etc/nagios/globus/
chown ${CHECK_MK_SITE} /usr/lib64/nagios/plugins/check_js

echo "Configuring access ..."
echo "Configured admins: $CHECK_MK_ADMINS"
sed -i "s|admin_users.*|admin_users = [$CHECK_MK_ADMINS]|" /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/multisite.mk

if [ -f /etc/check_mk/contacts.mk ]; then
    cp /etc/check_mk/contacts.mk /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/conf.d/wato/
fi
if [ -g /etc/check_mk/users.mk ]; then
    cp /etc/check_mk/users.mk /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/conf.d/wato/
fi

cp /etc/ncgx/templates/generic/handlers.cfg /opt/omd/sites/etf/etc/nagios/conf.d/

omd start
rm -f /opt/omd/sites/etf/etc/nagios/conf.d/handlers.cfg
echo "Configuring main.mk: $ETF_HOSTED_BY"
if [ -z "${ETF_HOSTED_BY}" ]; then
   echo "   Variable ETF_HOSTED_BY is not defined, not touching main.mk"
else
   if grep -q all_hosts /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/main.mk; then
      sed "s/all_hosts.*/all_hosts += [ \"${ETF_HOSTED_BY}\" ]/g" -i /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/main.mk
   else
      echo "all_hosts += [ \"${ETF_HOSTED_BY}\" ]" >> /opt/omd/sites/$CHECK_MK_SITE/etc/check_mk/main.mk
   fi
fi

echo "Configuring ETF ..."
if [ -z "${ETF_NAGIOS_HOST}" ]; then
    echo "   Variable ETF_NAGIOS_HOST is not defined, using hostname"
    ETF_NAGIOS_HOST=`hostname`
    echo "NAGIOS_HOST = \"${ETF_NAGIOS_HOST}\"" >> /etc/ncgx/ncgx.cfg
else
    echo "NAGIOS_HOST = \"${ETF_NAGIOS_HOST}\"" >> /etc/ncgx/ncgx.cfg
fi

su etf -c "ncgx --log | tee /opt/omd/sites/etf/var/log/ncgx.log"
su - etf -c "cmk -II; cmk -O"
if [ "${NSTREAM_ENABLED}" -eq "1" ] ; then
    echo "Nagios stream enabled ..."
else
    echo "Nagios stream disabled ..."
    /usr/bin/disable_nstream
fi

if [ "${ABRT_ENABLED}" -eq "1" ] ; then
    echo "Enabling abrt ..."
    sed -e "s/OpenGPGCheck = yes/OpenGPGCheck = no/g" -i /etc/abrt/abrt-action-save-package-data.conf
    /usr/sbin/abrtd
fi

echo "Fetching CMS credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /cms/Role=lcgadmin --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_lcgadmin --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --vo-fqan /cms/Role=production --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_production --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo cms --lifetime 24 --name NagiosRetrieve-ETF-cms -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Copying credentials ..."
/bin/cp -f /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/chown ${CHECK_MK_SITE} /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/chmod go+r /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec/probes/org.cms.glexec/testjob/tests/payloadproxy-t2
/bin/cp -f /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--cms-Role_production /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy
/bin/chown ${CHECK_MK_SITE} /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy
/bin/chmod go+r /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests/prodproxy

echo "Reloading crontab ..."
su etf -c "omd reload crontab"

echo "Starting Apache ..."
/usr/sbin/httpd -DFOREGROUND

